#ifndef _IMAGE_H
#define _IMAGE_H

#include "status.h"
#include <stdint.h>
#include <stdio.h>

typedef struct {
    uint8_t b, g, r;
} pixel_t;

typedef struct {
    uint32_t width, height;
    pixel_t *pixels;
} image_t;

pixel_t *pixel_of(image_t img, int32_t x, int32_t y);

enum read_status from_bmp(FILE *f, image_t *image);

image_t rotate(image_t image);

enum write_status to_bmp(FILE *f, image_t const *img);

image_t rotate_ang(image_t img, double angle);


#endif
