//
// Created by jakev on 22.12.2020.
//

#ifndef LAB8_SEPIA_H
#define LAB8_SEPIA_H

#include <inttypes.h>
#include "image.h"

void sepia_c_inplace(image_t* img);
void sepia_one(pixel_t *const pixel);


#endif //LAB8_SEPIA_H
