#include <stdint.h>
#include "image.h"
#include "bmp_struct.h"
#include "status.h"
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <math.h>


pixel_t* pixel_of(image_t img, int32_t x, int32_t y) {
    return &img.pixels[y * img.width + x];
}

enum read_status from_bmp(FILE *f, image_t *const image) {
    bmp_header_t header;
    size_t res;
    res = fread(&header, 1, sizeof(bmp_header_t), f);
    uint32_t width, height, space;

    if (res != sizeof(bmp_header_t)) return HEADER_ERR;

    if (header.bfType != 0x4d42 && header.bfType != 0x4349 && header.bfType != 0x5450 && header.biBitCount != 24) {
        return SIGNATURE_ERR;
    }

    width = header.biWidth;
    height = header.biHeight;
    image->width = width;
    image->height = height;


    pixel_t *data = malloc(height * width * sizeof(pixel_t));

    space = width % 4;
    res = 0;
    for (uint32_t i = 0; i < height; ++i) {
        res += fread(data + i * width, sizeof(pixel_t), (size_t) width, f);
        fseek(f, (size_t) space, SEEK_CUR);
    }

    if ((uint32_t) res != width * height) {
        free(data);
        return BITS_ERR;
    }

    image->pixels = data;
    return OK;

}

image_t rotate(image_t const image) {
    image_t new_image;
    uint32_t width = image.height;
    uint32_t height = image.width;
    pixel_t *data = malloc(height * width * sizeof(pixel_t));
    uint32_t m, n;
    m = n = 0;

    for (int32_t i = (int32_t) height - 1; i >= 0; --i) {
        for (uint32_t j = 0; j < width; j++) {
            *(data + m * width + n) = *(image.pixels + j * height + i);
            n++;
        }
        n = 0;
        m++;
    }
    new_image.width = width;
    new_image.height = height;
    new_image.pixels = data;

    printf("Rotate done\n");

    return new_image;
}


static void translate(double *tr_x, double *tr_y, double x, double y, double ang) {
    *tr_x = x * cos(ang) - y * sin(ang);
    *tr_y = x * sin(ang) + y * cos(ang);
}

static void calc_size(image_t source, double const ang, image_t *new_img) {
    double right = source.width / 2;
    double left = -right;
    double top = source.height / 2;
    double bottom = -top;

    double left_top_x, left_top_y;
    double left_bottom_x, left_bottom_y;
    double right_top_x, right_top_y;
    double right_bottom_x, right_bottom_y;

    translate(&left_top_x, &left_top_y, left, top, ang);
    translate(&left_bottom_x, &left_bottom_y, left, bottom, ang);
    translate(&right_bottom_x, &right_bottom_y, right, bottom, ang);
    translate(&right_top_x, &right_top_y, right, top, ang);

    new_img->width = (uint32_t) round(fmax(fmax(left_top_x, right_top_x), fmax(left_bottom_x, right_bottom_x))) * 2;
    new_img->height = (uint32_t) round(fmax(fmax(left_top_y, right_top_y), fmax(left_bottom_y, right_bottom_y))) * 2;
}

image_t rotate_ang(image_t img, double const angle) {
    double rad = -angle * M_PI / 180;
    double offset_x = img.width / 2.0;
    double offset_y = img.height / 2.0;
    double new_offset_x, new_offset_y;

    image_t new_img;

    calc_size(img, rad, &new_img);
    new_img.pixels = malloc(new_img.height * new_img.width * sizeof(pixel_t));
    new_offset_x = new_img.width / 2.0;
    new_offset_y = new_img.height / 2.0;

    for (uint32_t y = 0; y < new_img.height; y++) {
        for (uint32_t x = 0; x < new_img.width; x++) {
            double tr_x, tr_y;
            int32_t new_x, new_y;
            translate(&tr_x, &tr_y, x - new_offset_x, y - new_offset_y, rad);
            new_x = round(tr_x + offset_x);
            new_y = round(tr_y + offset_y);
            if (new_x >= 0 && new_x < img.width && new_y >= 0 && new_y < img.height) {
                new_img.pixels[y * new_img.width + x] = img.pixels[new_y * img.width + new_x];
            } else {
                new_img.pixels[y * new_img.width + x] = (pixel_t) {255, 255, 255};
            }
        }
    }

    return new_img;
}


enum write_status to_bmp(FILE *f, image_t const *img) {
    uint32_t width, height, space, sp_width;
    bmp_header_t header;
    memset(&header, 0, sizeof(header));
    width = img->width;
    height = img->height;
    space = width % 4;
    sp_width = sizeof(pixel_t) * width + space;
    header.bfType = 0x4d42;
    header.biSizeImage = 54 + height * sp_width;
    header.bfReserved = 0;
    header.biPlanes = 1;
    header.biSize = 40;
    header.bOffBits = 14 + header.biSize;
    header.biWidth = width;
    header.biHeight = height;
    header.biBitCount = 24;
    header.biCompression = 0;

    pixel_t *data;
    data = img->pixels;
    size_t res;
    res = fwrite(&header, 1, sizeof(bmp_header_t), f);
    if (res != sizeof(bmp_header_t)) return WRITE_ERR;
    res = 0;
    uint8_t trash[4] = {0};
    for (uint32_t i = 0; i < height; ++i) {
        res = fwrite(data, sizeof(pixel_t), (size_t) width, f);
        if (res != (size_t) width) return WRITE_ERR;
        if (space != 0) fwrite(trash, sizeof(uint8_t), space, f);
        data += (uint32_t) res;
    }
    return WR_OK;
}

