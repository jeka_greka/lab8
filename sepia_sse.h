//
// Created by jakev on 23.12.2020.
//

#ifndef LAB8_SEPIA_SSE_H
#define LAB8_SEPIA_SSE_H

#include "image.h"


void sepia_asm_inplace(image_t* img);

#endif //LAB8_SEPIA_SSE_H
