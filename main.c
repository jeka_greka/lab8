#include <stdio.h>
#include <sys/time.h>
#include <sys/resource.h>
#include "image.h"
#include <stdlib.h>
#include "sepia.h"
#include "sepia_sse.h"


int main(int argc, char *argv[]) {
    image_t image1;
    image_t image2;
    struct rusage r;
    struct timeval start;
    struct timeval end;
    FILE *file = fopen(argv[1], "rb");
    if (file == NULL) {
        printf("File doesn't exist\n");
        return FILE_OPEN_ERR;
    }

    int status = from_bmp(file, &image1);
    switch (status) {
        case OK:
            printf("File read done\n");
            break;
        case HEADER_ERR:
            printf("Invalid header\n");
            return HEADER_ERR;
            break;
        case SIGNATURE_ERR:
            printf("Invalid signature\n");
            return SIGNATURE_ERR;
            break;
        case BITS_ERR:
            printf("Invalid data of file\n");
            return BITS_ERR;
            break;
    }
    fclose(file);
    FILE *file1 = fopen(argv[1], "rb");
    from_bmp(file1, &image2);
    fclose(file1);

    getrusage(RUSAGE_SELF, &r);
    start = r.ru_utime;
    for (uint64_t i = 0; i < 1; ++i) {
        sepia_c_inplace(&image1);
    }
    getrusage(RUSAGE_SELF, &r);
    end = r.ru_utime;

    long res = (end.tv_sec - start.tv_sec) * 1000000L + end.tv_usec - start.tv_usec;
    printf("sepia_c done in %ld ms\n", res);

    char *name = "out_c.bmp";
    FILE *f = fopen(name, "wb");
    status = to_bmp(f, &image1);
    if (status == WR_OK) {
        printf("Write done\n");
        // return WR_OK;
    } else {
        printf("Write error\n");
        // return WRITE_ERR;
    }


    getrusage(RUSAGE_SELF, &r);
    start = r.ru_utime;

    for (uint64_t i = 0; i < 1; ++i) {
        sepia_asm_inplace(&image2);
    }

    getrusage(RUSAGE_SELF, &r);
    end = r.ru_utime;

    res = (end.tv_sec - start.tv_sec) * 1000000L + end.tv_usec - start.tv_usec;

    printf("sepia_asm done in %ld ms\n", res);

    name = "out_asm.bmp";
    FILE *f1 = fopen(name, "wb");
    status = to_bmp(f1, &image2);
    if (status == WR_OK) {
        printf("Write done\n");
        return WR_OK;
    } else {
        printf("Write error\n");
        return WRITE_ERR;
    }


}
