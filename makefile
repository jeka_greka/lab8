CC=gcc
CFLAGS=-c -O3
ASM=nasm
ASMFLAGS=-f elf64


all: main.o sepia_sse.o
	$(CC) build/*.o -o main -lm

main.o:
	$(CC) $(CFLAGS) sepia_sse.c  -o build/sepia_sse.o
	$(CC) $(CFLAGS) image.c  -o build/image.o
	$(CC) $(CFLAGS) sepia.c  -o build/sepia.o
	$(CC) $(CFLAGS) main.c  -o build/main.o

sepia_sse.o:
	$(ASM) $(ASMFLAGS) sepia.asm -o build/sepia_asm.o

clean:
	rm -f main
	rm -rf build/*.o
