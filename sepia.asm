%define c11 0.393
%define c12 0.769
%define c13 0.189
%define c21 0.349
%define c22 0.686
%define c23 0.168
%define c31 0.272
%define c32 0.534
%define c33 0.131


%macro fill_reg_1 2
mov eax, dword [rdi + %1]
mov [rsp], eax
mov [rsp + 4], eax
mov [rsp + 8], eax
mov eax, dword [rdi + 12 + %1]
mov [rsp + 12], eax
movups %2, [rsp]
%endmacro

%macro fill_reg_2 2
mov eax, dword [rdi + %1]
mov [rsp], eax
mov [rsp + 4], eax
mov eax, dword [rdi + 12 + %1]
mov [rsp + 8], eax
mov [rsp + 12], eax
movups %2, [rsp]
%endmacro

%macro fill_reg_3 2
mov eax, dword [rdi + %1]
mov [rsp], eax
mov eax, dword [rdi + 12 + %1]
mov [rsp + 4], eax
mov [rsp + 8], eax
mov [rsp + 12], eax
movups %2, [rsp]
%endmacro

%macro calculate 2
movaps xmm3, [xmm3_c_%1]
movaps xmm4, [xmm4_c_%1]
movaps xmm5, [xmm5_c_%1]

mulps xmm3, xmm0
mulps xmm4, xmm1
mulps xmm5, xmm2

addps xmm3, xmm4
addps xmm3, xmm5
minps xmm3, [max_values]

movaps [rsi+%2*16], xmm3

%endmacro



section .rodata
align 16
xmm3_c_1: dd c13, c23, c33, c13
align 16
xmm4_c_1: dd c12, c22, c32, c12
align 16
xmm5_c_1: dd c11, c21, c31, c11

align 16
xmm3_c_2: dd c23, c33, c13, c23
align 16
xmm4_c_2: dd c22, c32, c12, c22
align 16
xmm5_c_2: dd c21, c31, c11, c21

align 16
xmm3_c_3: dd c33, c13, c23, c33
align 16
xmm4_c_3: dd c32, c12, c22, c32
align 16
xmm5_c_3: dd c31, c11, c21, c31

align 16
max_values: dd 255.0, 255.0, 255.0, 255.0

global sepia_asm

section .text
sepia_asm:
sub rsp, 16

fill_reg_1 0, xmm0
fill_reg_1 4, xmm1
fill_reg_1 8, xmm2

calculate 1, 0

fill_reg_2 12, xmm0
fill_reg_2 16, xmm1
fill_reg_2 20, xmm3

calculate 2, 1

fill_reg_2 24, xmm0
fill_reg_2 28, xmm1
fill_reg_2 32, xmm3

calculate 3, 2

add rsp, 16
ret
