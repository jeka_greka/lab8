//
// Created by jakev on 23.12.2020.
//


#include "sepia_sse.h"
#include "sepia.h"


static float byte_to_float[256];

static void fill_array() {
    float number = 0.0f;
    for (uint32_t i = 0; i < 256; ++i) {
        byte_to_float[i] = number;
        number++;
    }
}

extern void sepia_asm(float input[12], float output[12]);

void sepia_asm_inplace(image_t *img) {
    float input[12];
    float output[12];
    uint32_t k;
    pixel_t pixel;
    fill_array();
    uint32_t count = img->width * img->height;
    uint32_t asm_count = count/4*4;
    uint32_t c_count = count - asm_count;
    for (k = 0; k < asm_count; k = k + 4) {
        for (uint32_t i = 0; i < 4; ++i) {
            pixel = img->pixels[i + k];
            input[i * 3] = byte_to_float[pixel.b];
            input[i * 3 + 1] = byte_to_float[pixel.g];
            input[i * 3 + 2] = byte_to_float[pixel.r];
        }

        sepia_asm(input, output);

        for (int j = 0; j < 4; ++j) {
            pixel.r = output[j * 3];
            pixel.g = output[j * 3 + 1];
            pixel.b = output[j * 3 + 2];
            img->pixels[j + k] = pixel;
        }
    }
    for (int l = 0; l < c_count; ++l) {
        pixel = img->pixels[l + asm_count];
        sepia_one(&pixel);
    }


}
